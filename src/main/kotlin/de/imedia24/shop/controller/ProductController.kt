package de.imedia24.shop.controller

import de.imedia24.shop.domain.product.PartialProductRequest
import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/products")
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @Operation(summary = "get a product by sku", description = "Returns a product with details")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "200", description = "Successful Operation"),
            ApiResponse(responseCode = "404", description = "No product found"),
        ]
    )
    @GetMapping("/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductBySku(
        @PathVariable("sku") sku: Long
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for product $sku")

        val product = productService.findProductBySku(sku)

        return product?.let { ResponseEntity.ok(it) } ?: ResponseEntity.notFound().build()

    }
    @Operation(summary = "get a product by skus params", description = "Returns a list of products with details")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "200", description = "Successful Operation"),
            ApiResponse(responseCode = "404", description = "No products found"),
        ]
    )
    @GetMapping("", produces = ["application/json;charset=utf-8"])
    fun findProductsBySkus(
        @RequestParam("skus") skus: Set<Long>
    ): ResponseEntity<List<ProductResponse>> {
        logger.info("Request for products $skus")
        val products = productService.findProductsBySkus(skus);
        return if(products.isNullOrEmpty()) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(products)
        }
    }

    @Operation(summary = "save a new product", description = "Returns the created product with details")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "200", description = "Successful Operation"),
        ]
    )
    @PostMapping("", produces = ["application/json;charset=utf-8"])
    fun addProduct(
       @Valid @RequestBody product : ProductRequest
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for saving product $product")
        val createdProduct = productService.saveProduct(product);
        return ResponseEntity(createdProduct, HttpStatus.CREATED);
    }

    @Operation(summary = "update some product fields", description = "update name and price and description if provided for a specific product")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "200", description = "Successful Operation"),
            ApiResponse(responseCode = "404", description = "No product found"),

        ]
    )
    @PatchMapping("/{sku}", produces = ["application/json;charset=utf-8"])
    fun updateProduct(
        @PathVariable("sku") sku:Long,
       @Valid @RequestBody partialProduct : PartialProductRequest
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for updating product $partialProduct")
        val updatedProduct = productService.updateProduct(sku,partialProduct);
        return updatedProduct?.let { ResponseEntity.ok(it) } ?: ResponseEntity.notFound().build()
    }

}
