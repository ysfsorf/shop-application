package de.imedia24.shop.service

import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.PartialProductRequest
import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import de.imedia24.shop.utils.Patcher
import org.springframework.stereotype.Service

@Service
class ProductService(private val productRepository: ProductRepository,private val patcher:Patcher) {

    fun findProductBySku(sku: Long): ProductResponse? {
        return productRepository.findBySku(sku)?.toProductResponse()
    }
    fun findProductsBySkus(skus: Set<Long>): List<ProductResponse>? {
        return productRepository.findAllBySkuIn(skus)?.map { it.toProductResponse() }
    }

    fun saveProduct(product:ProductRequest): ProductResponse? {
        return productRepository.save(product.toProductEntity()).toProductResponse()
    }

    fun updateProduct(sku:Long, partialProduct: PartialProductRequest): ProductResponse? {
        val oldProduct = productRepository.findBySku(sku)
        if (oldProduct!=null) {
            patcher.productPatcher(oldProduct, partialProduct.toProductEntity())
           return productRepository.save(oldProduct).toProductResponse()
        }
        return null;
    }
}
