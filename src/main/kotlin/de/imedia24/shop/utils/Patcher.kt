package de.imedia24.shop.utils

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.domain.product.PartialProductRequest
import org.springframework.stereotype.Component
import java.lang.reflect.Field

@Component
object Patcher {
    @Throws(IllegalAccessException::class)
    fun productPatcher(existingProduct: ProductEntity?, partialProduct: ProductEntity?) {

        val productClass: Class<*> = ProductEntity::class.java
        val internFields: Array<Field> = productClass.declaredFields
        println(internFields.size)
        for (field in internFields) {
            //CANT ACCESS IF THE FIELD IS PRIVATE
            field.setAccessible(true)
            //CHECK IF THE VALUE OF THE FIELD IS NOT NULL, IF NOT UPDATE EXISTING PRODUCT
            val value: Any? = field.get(partialProduct)
            if(value!=null)
                field.set(existingProduct, value)
            //MAKE THE FIELD PRIVATE AGAIN
            field.setAccessible(false)
        }
    }
}