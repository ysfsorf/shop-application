package de.imedia24.shop.domain.product

import de.imedia24.shop.db.entity.ProductEntity

import java.math.BigDecimal
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull
import javax.validation.constraints.Positive

data class ProductRequest(
    @field:NotEmpty(message = "name must not be blank")
    val name: String?,
    val description: String?,
    @field:NotNull(message = "price must be a positive value")
    @field:Positive(message = "price must be a positive value")
    val price: BigDecimal?,
    @field:NotNull(message = "quantity must be a positive value")
    @field:Positive(message = "quantity must be a positive value")
    val quantity: Int?
) {

        fun toProductEntity() = ProductEntity(
            name = name,
            description = description,
            price = price,
            quantity = quantity
        )

}
