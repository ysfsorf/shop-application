package de.imedia24.shop.domain.product

import de.imedia24.shop.db.entity.ProductEntity
import java.math.BigDecimal
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull
import javax.validation.constraints.Positive

data class PartialProductRequest(
    @field:NotBlank(message = "name must not be blank")
    val name: String?,
    @field:NotBlank(message = "name must not be blank")
    val description: String?,
    @field:Positive(message = "price must be positive value")
    val price: BigDecimal?,
){

    fun toProductEntity() = ProductEntity(
        name = name,
        description = description,
        price = price,
    )

}