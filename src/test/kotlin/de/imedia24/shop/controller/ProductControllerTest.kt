package de.imedia24.shop.controller
import com.fasterxml.jackson.databind.ObjectMapper
import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import java.math.BigDecimal

@ExtendWith(SpringExtension::class)
@WebMvcTest(ProductController::class)
class ProductControllerTest @Autowired constructor(private val mockMvc: MockMvc, private val mapper:ObjectMapper) {

    @MockBean
    lateinit var productService: ProductService


    @Test
    fun findProductsBySkus_givenAnExistingSkus_thenReturnsFoundProductsAndStatus200() {
        //Given
        val productResponse =    ProductResponse(1L,"test product","this my test product",
            BigDecimal(10),1
        )
        //When
        `when`(productService.findProductsBySkus(setOf(1))).thenReturn(listOf(productResponse
          ))
       val response=  mockMvc.perform(get("/products?skus=1"));

        //Then
        response.andExpect(status().isOk)
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(content().json(mapper.writeValueAsString(listOf(productResponse))))
    }
    @Test
    fun findProductsBySkus_givenNonExistingSkus_thenReturnsResponseWithStatus404() {
        //given
        val skus = setOf(404L);

        //When
        `when`(productService.findProductsBySkus(skus)).thenReturn(emptyList())
       val response =  mockMvc.perform(get("/products?skus=404"));

        //Then
        response.andExpect(status().isNotFound)
        verify(productService).findProductsBySkus(setOf(404))

    }


    @Test
    fun findProductsBySkus_givenWrongQueryType_thenReturnsResponseWithStatus400() {
        val response = mockMvc.perform(get("/products?skus=productName"))
        response.andExpect(status().isBadRequest)
    }
    @Test
    fun addProduct_givenProductRequest_shouldCreateProductAndReturnsStatus201() {
        // Given
        val productRequest = ProductRequest("Test Product", "This is a test product", BigDecimal(100),3)
        val productResponse = ProductResponse(1L, "Test Product", "This is a test product",BigDecimal(100),3)
        `when`(productService.saveProduct(productRequest)).thenReturn(productResponse)

        // When
        val response = mockMvc.perform(
            post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(productRequest)))

        // then
        verify(productService).saveProduct(productRequest)
        response.andExpect(status().isCreated)
            .andExpect(content().json(mapper.writeValueAsString(productResponse)))
    }

    @Test
    fun addProduct_givenProductRequestWithNonValidField_shouldReturnBadRequestWithStatus400() {
        // Given
        val productRequest = ProductRequest("", "This is a test product", BigDecimal(-100),-3)
        val errors : HashMap<String, String> = HashMap();
        errors["name"] = "name must not be blank";
        errors["price"] = "price must be a positive value";
        errors["quantity"] = "quantity must be a positive value";
        // When
        val response = mockMvc.perform(
            post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(productRequest)))

        // then
        response.andExpect(status().isBadRequest)
            .andExpect(content().json(mapper.writeValueAsString(errors)))

    }
}