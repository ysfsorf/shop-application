# iMedia24 Coding challenge

### Reference Documentation
For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)
* [Official Kotlin documentation](https://kotlinlang.org/docs/home.html)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.3/gradle-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.3/gradle-plugin/reference/html/#build-image)
* [Flyway database migration tool](https://flywaydb.org/documentation/)

### Additional Links
These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)



## How to run the app using docker

 Below are the instructions on how to proceed.

### Building the Spring Boot Application

Before building the Docker image, ensure that you have built the Spring Boot application JAR file using Gradle. Run the following command in your project directory:

```bash
./gradlew build
```

This command compiles the source code, runs tests, and packages the application into a JAR file.


### Building the Docker Image
To build the Docker image, use the provided Dockerfile. Ensure that you have Docker installed on your system.

Navigate to the root directory of the project where you have the Dockerfile and run the following command:

```bash
docker build -t your-custom-name-here .
```

Running the Docker Container
Once the Docker image is built, you can run the container using the following command:

```bash
docker run --rm -p 8080:8080 your-custom-name-here
```

This command starts the Docker container and exposes port 8080 of the container to port 8080 on your host machine.